cmake_minimum_required(VERSION 3.12)
project(opencv-yuv)

find_package(OpenCV REQUIRED)

find_package(Boost COMPONENTS system filesystem REQUIRED)
include_directories(${Boost_INCLUDE_DIR})

set(CMAKE_CXX_STANDARD 17)

set(SOURCE_FILES
        main.cpp
        yuv.cpp
        yuv.h)

add_executable(opencv-yuv ${SOURCE_FILES})
target_link_libraries(opencv-yuv
        ${OpenCV_LIBS}
        ${Boost_LIBRARIES}
        )